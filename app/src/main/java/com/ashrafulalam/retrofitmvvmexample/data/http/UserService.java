package com.ashrafulalam.retrofitmvvmexample.data.http;

import com.ashrafulalam.retrofitmvvmexample.model.User;
import com.ashrafulalam.retrofitmvvmexample.utils.Constants;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface UserService {

    @GET(Constants.USERS_ENDPOINT)
    Call<List<User>> getUsers();
}
