package com.ashrafulalam.retrofitmvvmexample.data.http;

import com.ashrafulalam.retrofitmvvmexample.utils.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitHelper {

    public static Retrofit getRetrofit(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;

    }

    public static UserService getuserService() {
        return getRetrofit().create(UserService.class);
    }
}
