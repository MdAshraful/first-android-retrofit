package com.ashrafulalam.retrofitmvvmexample.ui.main;

import android.util.Log;

import com.ashrafulalam.retrofitmvvmexample.data.http.RetrofitHelper;
import com.ashrafulalam.retrofitmvvmexample.data.http.UserService;
import com.ashrafulalam.retrofitmvvmexample.model.User;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainViewModel extends ViewModel {
    private static final String TAG = "MainViewModel";

    private MutableLiveData<List<User>> userLiveData =new MutableLiveData<>();

    public MainViewModel(){
        generateUserData();

    }

    public LiveData<List<User>> getUserLiveData(){
        return userLiveData;
    }

    private void generateUserData(){

        UserService userService = RetrofitHelper.getuserService();

        userService.getUsers().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                List<User> userList = response.body();
                userLiveData.setValue(userList);
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);

            }
        });
    }
}
