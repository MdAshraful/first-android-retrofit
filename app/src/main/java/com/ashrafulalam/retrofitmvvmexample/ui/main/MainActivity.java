package com.ashrafulalam.retrofitmvvmexample.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.util.Log;

import com.ashrafulalam.retrofitmvvmexample.R;
import com.ashrafulalam.retrofitmvvmexample.databinding.ActivityMainBinding;
import com.ashrafulalam.retrofitmvvmexample.model.User;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    ActivityMainBinding binding;
    MainViewModel viewModel;
    UserAdapter userAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        binding.recyclerViewId.setLayoutManager(new LinearLayoutManager(this));

        userAdapter=new UserAdapter(this);
        binding.recyclerViewId.setAdapter(userAdapter);

        viewModel.getUserLiveData().observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(List<User> users) {
                Log.d(TAG, "onChanged: "+users.size());
                userAdapter.setUserList(users);
            }
        });


    }
}
