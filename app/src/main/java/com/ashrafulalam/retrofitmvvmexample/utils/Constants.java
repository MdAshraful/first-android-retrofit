package com.ashrafulalam.retrofitmvvmexample.utils;

public interface Constants {

    String BASE_URL ="https://jsonplaceholder.typicode.com/";
    String USERS_ENDPOINT ="users";
}
